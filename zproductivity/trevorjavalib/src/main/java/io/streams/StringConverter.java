package io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by Trevor.Rydalch on 6/23/2017.
 */

public class StringConverter
{
    public static String readString(InputStream is) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    public static void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }

}
