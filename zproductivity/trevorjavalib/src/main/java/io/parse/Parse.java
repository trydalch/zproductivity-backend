package io.parse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Parse
{
    public static class Parser
    {

    }

    public static Date parseStringToDate(String str)
    {
        assert str != null;
        assert !str.isEmpty();

        List<DateFormat> formats = new ArrayList<>();
        formats.add(new SimpleDateFormat("MM.dd.yyyy"));
        formats.add(new SimpleDateFormat("MM/dd/yyyy"));
        formats.add(new SimpleDateFormat("MM/dd"));
        Date date = new Date();
        for (DateFormat format : formats)
        {
            try
            {
                date = format.parse(str);
                return date;
            }
            catch (ParseException e)
            {
                // Not the correct format.
            }
        }
        return date;
    }

    public static Date parseStringToDate(DateFormat format, String str)
    {
        assert str != null;
        assert !str.isEmpty();
        Date date = new Date();
        try
        {
            date = format.parse(str);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }
}
