package serviceAPIs;

import comm.requests.RegisterRequest;
import comm.results.RegisterResult;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.File;

import dao.DAOControl;
import exceptions.DatabaseException;
import model.User;

/**
 * Created by Trevor.Rydalch on 6/23/2017.
 */

public class testUserServiceAPI
{
    private RegisterRequest m_request;
    private UserServiceAPI m_service;
    private DAOControl m_daoControl;

    private String m_dbName = "data" + File.separator + "db" + File.separator + "testZProductivity.sqlite";
    private User m_user;

    @Before
    public void setUp()
    {
        m_request = new RegisterRequest();
        m_service = new UserServiceAPI();
        m_daoControl = new DAOControl();
        m_daoControl.setDB(m_dbName);
        m_service.setDAO(m_daoControl);
        m_user = new User();
    }

    @Test
    public void testValidRegister()
    {
        m_request.setPassword("password");
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);

        assertNotNull(result.getAuthToken());
    }

    @Test
    public void testMissingUserNameRegister()
    {
        m_request.setPassword("password");
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        result = m_service.createUser(m_request);

        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testMissingPasswordRegister()
    {
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);
        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testMissingFNameRegister()
    {
        m_request.setPassword("password");
        m_request.setEmail("test@email.com");
        m_request.setLastName("Register");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);
        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testMissingLNameRegister()
    {
        m_request.setPassword("password");
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);
        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testMissingAgeRegister()
    {
        m_request.setPassword("password");
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);
        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testMissingGenderRegister()
    {
        m_request.setPassword("password");
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);
        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testMissingEmailRegister()
    {
        m_request.setPassword("password");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;
            m_request.setUserName("TEST" + nextID);

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }

        result = m_service.createUser(m_request);
        assertEquals("Please enter all required fields.", result.getMessage());
    }

    @Test
    public void testUsernameTaken()
    {
        m_request.setUserName("testMain");
        m_request.setPassword("invalid");
        m_request.setEmail("test@email.com");
        m_request.setFirstName("New");
        m_request.setLastName("Register");
        m_request.setAge(24);
        m_request.setGender("M");
        RegisterResult result = new RegisterResult();
        try
        {
            m_daoControl.openConnection();
            int nextID = m_daoControl.readLastID(m_user) + 1;

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(result.getAuthToken() != null);
        }
        result = m_service.createUser(m_request);

        assertEquals("Username is taken", result.getMessage());
    }
}
