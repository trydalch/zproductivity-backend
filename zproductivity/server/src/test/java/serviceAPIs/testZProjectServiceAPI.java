package serviceAPIs;

import org.junit.*;
import static org.junit.Assert.*;
import java.io.File;


import comm.requests.CreateZProjectRequest;
import comm.results.CreateZProjectResult;
import dao.DAOControl;

/**
 * Created by Trevor.Rydalch on 6/23/2017.
 */

public class testZProjectServiceAPI
{
    private CreateZProjectRequest m_request;
    private CreateZProjectResult m_result;
    private ZProjectServiceAPI m_zProjectServiceAPI;
    private DAOControl m_daoControl;

    private String m_dbName = "data" + File.separator + "db" + File.separator + "testZProductivity.sqlite";


    @Before
    public void setUp()
    {
        m_request = new CreateZProjectRequest();
        m_result = new CreateZProjectResult();
        m_zProjectServiceAPI = new ZProjectServiceAPI();
        m_daoControl = new DAOControl();
        m_daoControl.setDB(m_dbName);
        m_zProjectServiceAPI.setDAO(m_daoControl);
    }

    @Test
    public void testValidCreateZProject()
    {
        m_request.setAuthToken("TESTAUTH");
        m_request.setName("TESTProject");
        m_request.setDateDue("04.21.2018");
        m_request.setTimeEstimated(5);
        m_result = m_zProjectServiceAPI.createZProject(m_request);

        assertEquals("createZProject successful", m_result.getMessage());
    }

    @Test
    public void testMissingAuthToken()
    {
        m_request.setName("TESTProject");
        m_request.setDateDue("04.21.2018");
        m_request.setTimeEstimated(5);
        m_result = m_zProjectServiceAPI.createZProject(m_request);

        assertEquals("Missing Authorization Token", m_result.getMessage());
    }

    @Test
    public void testInvalidAuthToken()
    {
        m_request.setAuthToken("INVALID");
        m_request.setName("TESTProject");
        m_request.setDateDue("04.21.2018");
        m_request.setTimeEstimated(5);
        m_result = m_zProjectServiceAPI.createZProject(m_request);

        assertEquals("Invalid Authorization Token", m_result.getMessage());
    }

    @Test
    public void testInvalidDateFormat()
    {
        m_request.setAuthToken("TESTAUTH");
        m_request.setName("TESTProject");
        m_request.setDateDue("04/21/2018");
        m_request.setTimeEstimated(5);
        m_result = m_zProjectServiceAPI.createZProject(m_request);

        //TODO Test not passing.
//        assertEquals("", m_result.getMessage());
    }
}
