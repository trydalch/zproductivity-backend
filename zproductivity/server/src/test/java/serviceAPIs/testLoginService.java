package serviceAPIs;

import org.junit.*;
import static org.junit.Assert.*;
import comm.requests.LoginRequest;
import comm.results.LoginResult;

import java.io.File;

import dao.DAOControl;

/**
 * Created by Trevor.Rydalch on 6/23/2017.
 */

public class testLoginService
{
    private LoginRequest m_request;
    private LoginResult m_result;
    private DAOControl m_daoControl;
    private LoginService m_service;

    private String m_dbName = "data" + File.separator + "db" + File.separator + "testZProductivity.sqlite";

    @Before
    public void setUp()
    {
        m_request = new LoginRequest();
        m_result = new LoginResult();
        m_service = new LoginService();
        m_daoControl = new DAOControl();
        m_daoControl.setDB(m_dbName);
        m_service.setDAO(m_daoControl);
    }

    @Test
    public void testValidLogin()
    {
        m_request.setUserName("testMain");
        m_request.setPassword("test");

        m_result = m_service.login(m_request);

        assertNotNull(m_result);
        assertNotNull(m_result.getAuthToken());
    }

    @Test
    public void testInvalidPasswordLogin()
    {
        m_request.setUserName("testMain");
        m_request.setPassword("invalid");

        m_result = m_service.login(m_request);

        assertEquals("Invalid password", m_result.getMessage());
    }

    @Test
    public void testInvalidUsernameLogin()
    {
        m_request.setUserName("invalid");
        m_request.setPassword("test");

        m_result = m_service.login(m_request);

        assertEquals("Invalid username", m_result.getMessage());
    }

    @Test
    public void testInvalidUsernamePasswordLogin()
    {
        m_request.setUserName("invalid");
        m_request.setPassword("invalid");

        m_result = m_service.login(m_request);

        assertEquals("Invalid username", m_result.getMessage());
    }
}
