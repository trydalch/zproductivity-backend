package webAPIs;

import com.google.gson.Gson;
import comm.requests.DeleteZProjectRequest;
import comm.results.DeleteZProjectResult;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.net.HttpURLConnection;

import io.streams.StringConverter;
import serviceAPIs.ZProjectServiceAPI;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class DeleteZProjectHandler implements HttpHandler
{
    private ZProjectServiceAPI m_zProjectServiceAPI = new ZProjectServiceAPI();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        if (httpExchange.getRequestMethod().toUpperCase().equals("POST") && httpExchange.getRequestHeaders().containsKey("Authorization"))
        {
            String reqData = StringConverter.readString(httpExchange.getRequestBody());

            Gson gson = new Gson();
            DeleteZProjectRequest deleteZProjectRequest = gson.fromJson(reqData, DeleteZProjectRequest.class);
            deleteZProjectRequest.setAuthToken(httpExchange.getRequestHeaders().getFirst("Authorization"));

            DeleteZProjectResult deleteZProjectResult = m_zProjectServiceAPI.deleteZProject(deleteZProjectRequest);

            String jsonStr = gson.toJson(deleteZProjectResult);

            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            StringConverter.writeString(jsonStr, httpExchange.getResponseBody());

            httpExchange.getResponseBody().close();
        }
    }
}
