package webAPIs;

import com.google.gson.Gson;
import comm.requests.CreateZProjectRequest;
import comm.results.CreateZProjectResult;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import model.AuthToken;
import serviceAPIs.ZProjectServiceAPI;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class WebZProjectAPI implements HttpHandler {

    private ZProjectServiceAPI m_ZProjectServiceAPI = new ZProjectServiceAPI();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        if (httpExchange.getRequestMethod().toUpperCase().equals("POST"))
        {
            if (httpExchange.getRequestHeaders().containsKey("Authorization"))
            {
                AuthToken authToken = new AuthToken();
                authToken.setAuthId(httpExchange.getRequestHeaders().getFirst("Authorization"));

                // Get requestBody containing username and password and convert to String
                Gson gson = new Gson();
                InputStream reqBody = httpExchange.getRequestBody();
                String reqData = readString(reqBody);

                // Convert from a String to LoginRequest Java object
                CreateZProjectRequest createZProjectRequest = gson.fromJson(reqData, CreateZProjectRequest.class);
                createZProjectRequest.setAuthToken(authToken.getAuthId());
                // Process CreateZProjectRequest
                CreateZProjectResult createZProjectResult = m_ZProjectServiceAPI.createZProject(createZProjectRequest);

                // Convert to Json
                String jsonStr = gson.toJson(createZProjectResult);
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                OutputStream respBody = httpExchange.getResponseBody();
                writeString(jsonStr, respBody);

                respBody.close();
            }
            else
            {
                // Doesn't contain an authorization token
            }


        }
        else
        {
            // Invalid request method
        }
    }

    /*
The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }

}
