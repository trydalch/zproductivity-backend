package webAPIs;

import com.google.gson.Gson;
import comm.requests.RegisterRequest;
import comm.results.RegisterResult;
import serviceAPIs.UserServiceAPI;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class WebUserAPI implements HttpHandler
{
    private UserServiceAPI m_userServiceAPI = new UserServiceAPI();
    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        String reqMethod = httpExchange.getRequestMethod().toUpperCase();
        if (reqMethod.equals("POST"))
        {
            Gson gson = new Gson();
            InputStream reqBody = httpExchange.getRequestBody();

            String reqData = readString(reqBody);
            RegisterRequest registerRequest = gson.fromJson(reqData, RegisterRequest.class);

            RegisterResult registerResult = m_userServiceAPI.createUser(registerRequest);
            String jsonStr = gson.toJson(registerResult);

            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            OutputStream respBody = httpExchange.getResponseBody();
            writeString(jsonStr, respBody);

            respBody.close();
        }
    }

    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
