package webAPIs;

import com.google.gson.Gson;
import comm.requests.ReadZProjectRequest;
import comm.results.ReadZProjectResult;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.rmi.ServerException;

import io.streams.StringConverter;
import serviceAPIs.ZProjectServiceAPI;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class ReadZProjectHandler implements HttpHandler
{
    private ZProjectServiceAPI m_zProjectServiceAPI = new ZProjectServiceAPI();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        try
        {
            if(httpExchange.getRequestMethod().toUpperCase().equals("GET"))
            {
                if (httpExchange.getRequestHeaders().containsKey("Authorization"))
                {
                    InputStream reqBody = httpExchange.getRequestBody();
                    String reqData = StringConverter.readString(reqBody);

                    Gson gson = new Gson();
                    ReadZProjectRequest request = new ReadZProjectRequest();
                    if (! reqData.isEmpty())
                    {
                        request = gson.fromJson(reqData, ReadZProjectRequest.class);
                    }
                    request.setUserID(m_zProjectServiceAPI.readUserIDFromAuth(httpExchange.getRequestHeaders().getFirst("Authorization")));
                    ReadZProjectResult result = m_zProjectServiceAPI.readZProject(request);

                    String jsonStr = gson.toJson(result);

                    //Send headers and responsebody containing data
                    httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                    OutputStream respBody = httpExchange.getResponseBody();
                    StringConverter.writeString(jsonStr, respBody);
                    respBody.close();
                }
            }
            else
            {
                //Invalid request method
            }
        }
        catch (ServerException ex)
        {

        }
    }
}
