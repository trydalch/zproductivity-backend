package webAPIs;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Created by Trevor.Rydalch on 5/27/2017.
 */

public class DefaultHandler implements HttpHandler
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }
    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("DefaultHandler", "handle");
        String fileDir = "data\\web";

        URI uri = httpExchange.getRequestURI();
        logger.info(uri.toString() + " requested");
        String filePathStr = new String();
        if(uri.toString().equals("/"))
        {
            filePathStr = fileDir + "\\index.html";
        }
        else
        {
            // index.html has already been sent, the client has now asked for additional files.
            filePathStr = fileDir;

            // parse to identify file being requested.
            Scanner scanner = new Scanner(uri.toString());
            scanner.useDelimiter("/");
            while (scanner.hasNext())
            {
                filePathStr += "\\" + scanner.next();
            }
        }
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
        Path filePath = FileSystems.getDefault().getPath(filePathStr);
        OutputStream responseBody = httpExchange.getResponseBody();
        Files.copy(filePath, responseBody);
        responseBody.close();
        logger.exiting("DefaultHandler", "handle");
    }
}
