package webAPIs;

import com.google.gson.Gson;
import comm.requests.UpdateZProjectRequest;
import comm.results.UpdateZProjectResult;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import io.streams.StringConverter;
import model.AuthToken;
import serviceAPIs.ZProjectServiceAPI;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class UpdateZProjectHandler implements HttpHandler
{
    private ZProjectServiceAPI m_zProjectService = new ZProjectServiceAPI();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {

        if (httpExchange.getRequestMethod().toUpperCase().equals("POST"))
        {
            if (httpExchange.getRequestHeaders().containsKey("Authorization"))
            {
                AuthToken authToken = new AuthToken();
                authToken.setAuthId(httpExchange.getRequestHeaders().getFirst("Authorization"));

                // Get requestBody containing username and password and convert to String
                Gson gson = new Gson();
                InputStream reqBody = httpExchange.getRequestBody();
                String reqData = StringConverter.readString(reqBody);

                // Convert from a String to LoginRequest Java object
                UpdateZProjectRequest updateZProjectRequest = gson.fromJson(reqData, UpdateZProjectRequest.class);
                updateZProjectRequest.setAuthToken(authToken.getAuthId());
                // Process CreateZProjectRequest
                UpdateZProjectResult createZProjectResult = m_zProjectService.updateZProject(updateZProjectRequest);

                // Convert to Json
                String jsonStr = gson.toJson(createZProjectResult);
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                OutputStream respBody = httpExchange.getResponseBody();
                StringConverter.writeString(jsonStr, respBody);

                respBody.close();
            }
            else
            {
                // Doesn't contain an authorization token
            }


        }
        else
        {
            // Invalid request method
        }
    }
}
