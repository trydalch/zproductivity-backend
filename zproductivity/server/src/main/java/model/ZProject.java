package model;

import java.util.Date;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class ZProject
{
    private int m_ZPID;
    private String m_ZPName;
    private double m_timeEstimated;
    private double m_timeTracked;
    private Date m_dateDue;
    private Date m_dateStart;
    private Date m_dateCreated;
    private Date m_dateFinished;
    private String m_context;
    private int m_userID;


    public int getID()
    {
        return m_ZPID;
    }

    public void setZPID(int id)
    {
        m_ZPID = id;
    }

    public String getZPName()
    {
        return m_ZPName;
    }

    public void setZPName(String ZPName)
    {
        m_ZPName = ZPName;
    }

    public double getTimeEstimated()
    {
        return m_timeEstimated;
    }

    public void setTimeEstimated(double timeEstimated)
    {
        m_timeEstimated = timeEstimated;
    }

    public double getTimeTracked()
    {
        return m_timeTracked;
    }

    public void setTimeTracked(double timeTracked)
    {
        m_timeTracked = timeTracked;
    }

    public Date getDateDue()
    {
        return m_dateDue;
    }

    public void setDateDue(Date dateDue)
    {
        m_dateDue = dateDue;
    }

    public Date getDateStart()
    {
        return m_dateStart;
    }

    public void setDateStart(Date dateStart)
    {
        m_dateStart = dateStart;
    }

    public int getUserID()
    {
        return m_userID;
    }

    public void setUserID(int userID)
    {
        m_userID = userID;
    }

    public Date getDateCreated()
    {
        return m_dateCreated;
    }

    public void setDateCreated(Date dateCreated)
    {
        m_dateCreated = dateCreated;
    }

    public Date getDateFinished()
    {
        return m_dateFinished;
    }

    public void setDateFinished(Date dateFinished)
    {
        m_dateFinished = dateFinished;
    }

    public String getContext()
    {
        return m_context;
    }

    public void setContext(String context)
    {
        m_context = context;
    }

    // TODO Override toString
    @Override
    public String toString()
    {
        return super.toString();
    }

    // TODO OVerride hashCode
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }
}
