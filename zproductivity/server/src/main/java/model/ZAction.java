package model;

import java.util.Date;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class ZAction
{
    private int m_id;
    private String m_name;
    private double m_timeEstimated;
    private double m_timeTracked;
    private Date m_dateDue;
    private Date m_dateStart;
    private Date m_dateFinished;
    private Date m_dateCreated;
    private String m_context;
    private int m_parentID;
    private int m_userID;

    public int getID()
    {
        return m_id;
    }

    public void setID(int id)
    {
        m_id = id;
    }

    public String getName()
    {
        return m_name;
    }

    public void setName(String name)
    {
        m_name = name;
    }

    public double getTimeEstimated()
    {
        return m_timeEstimated;
    }

    public void setTimeEstimated(double timeEstimated)
    {
        m_timeEstimated = timeEstimated;
    }

    public double getTimeTracked()
    {
        return m_timeTracked;
    }

    public void setTimeTracked(double timeTracked)
    {
        m_timeTracked = timeTracked;
    }

    public Date getDateDue()
    {
        return m_dateDue;
    }

    public void setDateDue(Date dateDue)
    {
        m_dateDue = dateDue;
    }

    public Date getDateStart()
    {
        return m_dateStart;
    }

    public void setDateStart(Date dateStart)
    {
        m_dateStart = dateStart;
    }

    public Date getDateFinished()
    {
        return m_dateFinished;
    }

    public void setDateFinished(Date dateFinished)
    {
        m_dateFinished = dateFinished;
    }

    public Date getDateCreated()
    {
        return m_dateCreated;
    }

    public void setDateCreated(Date dateCreated)
    {
        m_dateCreated = dateCreated;
    }

    public int getUserID()
    {
        return m_userID;
    }

    public void setUserID(int userID)
    {
        m_userID = userID;
    }

    public int getParentID()
    {
        return m_parentID;
    }

    public void setParentID(int parentID)
    {
        m_parentID = parentID;
    }

    public String getContext()
    {
        return m_context;
    }

    public void setContext(String context)
    {
        m_context = context;
    }
}
