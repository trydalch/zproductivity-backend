package model;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class User
{



    private int m_id;
    private String m_userName;
    private String m_password;
    private String m_email;
    private String m_firstName;
    private String m_lastName;
    private String m_gender;
    private int m_age;

    public User(){};

    public User(String username, String password, String email, String firstName, String lastName, String gender, int age)
    {
        this.m_userName = username;
        this.m_password = password;
        this.m_email = email;
        this.m_firstName = firstName;
        this.m_lastName = lastName;
        this.m_gender = gender;
        this.m_age = age;
    }

    public int getID()
    {
        return m_id;
    }

    public void setID(int id)
    {
        m_id = id;
    }

    public String getUserName()
    {
        return m_userName;
    }

    public void setUserName(String userName)
    {
        m_userName = userName;
    }

    public String getPassword()
    {
        return m_password;
    }

    public void setPassword(String password)
    {
        m_password = password;
    }

    public String getEmail()
    {
        return m_email;
    }

    public void setEmail(String email)
    {
        m_email = email;
    }

    public String getFirstName()
    {
        return m_firstName;
    }

    public void setFirstName(String firstName)
    {
        m_firstName = firstName;
    }

    public String getLastName()
    {
        return m_lastName;
    }

    public void setLastName(String lastName)
    {
        m_lastName = lastName;
    }

    public String getGender()
    {
        return m_gender;
    }

    public void setGender(String gender)
    {
        m_gender = gender.toUpperCase();
    }

    public int getAge()
    {
        return m_age;
    }

    public void setAge(int age)
    {
        m_age = age;
    }
}
