package model;

import java.sql.Time;
import java.util.UUID;

/**
 * An Authorization Token consisting of a unique AuthTok String
 * <pre>
 *     <b>Domain</b>
 *          m_sAuthTok  : String
 *          m_sPersondId : String
 * </pre>
 */
public class AuthToken
{
    /**
     * Default constructor for an AuthToken object
     * @param usr
     */
    public AuthToken(User usr)
    {
        this.m_UserID = usr.getID();
        m_iTimeCreated = System.currentTimeMillis();
        genRandomAuthToken();
    }

    public AuthToken(){};
    private String m_sAuthTok;
    private int m_UserID;
    private long m_iTimeCreated;

    public String getAuthId() {
        return m_sAuthTok;
    }

    public void setAuthId(String token) {
        this.m_sAuthTok = token;
    }

    public int getUserID() { return m_UserID; }

    public void setUserID(int usrID) { this.m_UserID = usrID; }

    public long getTimeCreated() {
        return m_iTimeCreated;
    }

    public void setTimeCreated(long m_iTimeCreated) {
        this.m_iTimeCreated = m_iTimeCreated;
    }

    public void genRandomAuthToken()
    {
        m_sAuthTok = UUID.randomUUID().toString();
    }

}
