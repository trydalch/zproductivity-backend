package serviceAPIs;

import comm.requests.CreateZActionRequest;
import comm.requests.DeleteZActionRequest;
import comm.requests.ReadZActionRequest;
import comm.requests.UpdateZActionRequest;
import comm.results.CreateZActionResult;
import comm.results.DeleteZActionResult;
import comm.results.ReadZActionResult;
import comm.results.UpdateZActionResult;
import dao.DAOControl;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class ZActionServiceAPI
{
    private DAOControl m_daoControl = new DAOControl();

    public CreateZActionResult createZAction(CreateZActionRequest request)
    {
        //TODO Implement.
        return null;
    }

    public ReadZActionResult readZAction(ReadZActionRequest request)
    {
        //TODO Implement.
        return null;
    }

    public UpdateZActionResult updateZAction(UpdateZActionRequest request)
    {
        //TODO Implement.
        return null;
    }

    public DeleteZActionResult deleteZAction(DeleteZActionRequest request)
    {
        //TODO Implement
        return null;
    }


}
