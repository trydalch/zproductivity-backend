package serviceAPIs;

import comm.requests.RegisterRequest;
import comm.results.RegisterResult;

import dao.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import model.User;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class UserServiceAPI
{

    private DAOControl m_daoControl = new DAOControl();

    public RegisterResult createUser(RegisterRequest request)
    {
        RegisterResult result = new RegisterResult();

        // Does the request contain all necessary values?
        if (!checkForMissingValues(request))
        {
            // No. Return a result with the error message below.
            result.setMessage("Please enter all required fields.");
            return result;
        }
        boolean addAuth = false;
        boolean addUsr = false;
        try
        {
            m_daoControl.openConnection();
            User usr = new User(request.getUserName(), request.getPassword(), request.getEmail(), request.getFirstName(), request.getLastName(), request.getGender(), request.getAge());
            int lastUserID = m_daoControl.readLastID(usr);
            usr.setID(++lastUserID);
            addUsr = m_daoControl.createUser(usr);
            AuthToken authToken = new AuthToken(usr);
            addAuth = m_daoControl.createAuthToken(authToken);

            if (addUsr && addAuth)
            {
                result.setUserID(usr.getID());
                request.setUserName(usr.getUserName());
                result.setAuthToken(authToken.getAuthId());
            }
        }
        catch (DatabaseException e)
        {
            result.setMessage(e.getMessage());
        }
        finally
        {
            m_daoControl.closeConnection(addAuth && addUsr);
        }
        return result;
    }

    private boolean checkForMissingValues(RegisterRequest request)
    {
        if (request.getFirstName() == null || request.getLastName() == (null) ||
            request.getUserName() == (null) || request.getPassword() == (null) ||
            request.getEmail() == (null) || request.getAge() == 0 || request.getGender() == null)
        {
            // Some value is null
            return false;
        }
        else if(request.getFirstName().equals("") || request.getLastName().equals("") || request.getUserName().equals("") || request.getPassword().equals("") || request.getEmail().equals(""))
        {
            // Some value is empty, but not null
            return false;
        }
        // No values are empty or null
        return true;
    }

    public void setDAO(DAOControl DAO)
    {
        m_daoControl = DAO;
    }
}
