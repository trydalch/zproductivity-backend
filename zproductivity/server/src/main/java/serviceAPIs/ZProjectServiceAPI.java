package serviceAPIs;

import comm.requests.CreateZProjectRequest;
import comm.requests.DeleteZProjectRequest;
import comm.requests.ReadZProjectRequest;
import comm.requests.UpdateZProjectRequest;
import comm.results.CreateZProjectResult;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import comm.results.DeleteZProjectResult;
import comm.results.ReadZProjectResult;
import comm.results.UpdateZProjectResult;
import dao.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import model.ZProject;

import static io.parse.Parse.parseStringToDate;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class ZProjectServiceAPI
{

    private DAOControl m_daoControl = new DAOControl();

    public CreateZProjectResult createZProject(CreateZProjectRequest request)
    {
        CreateZProjectResult result = new CreateZProjectResult();
        boolean successfulCreate = false;
        try
        {
            ZProject newZProj = new ZProject();

            m_daoControl.openConnection();

            // Check auth token and if valid, get the userID
            AuthToken authToken = m_daoControl.readAuthToken(request.getAuthToken());

            int lastID = (m_daoControl.readLastID(newZProj));
            newZProj.setZPID(lastID + 1);
            newZProj.setZPName(request.getName());
            newZProj.setTimeEstimated(request.getTimeEstimated());
            newZProj.setTimeTracked(request.getTimeTracked());


            if (request.getDateDue() != null && !request.getDateDue().isEmpty()) newZProj.setDateDue(parseStringToDate(request.getDateDue()));
            if (request.getDateStart() != null && !request.getDateStart().isEmpty()) newZProj.setDateStart(parseStringToDate(request.getDateDue()));
            if (request.getDateFinished() != null && !request.getDateFinished().isEmpty()) newZProj.setDateFinished(parseStringToDate(request.getDateDue()));
            if (request.getDateCreated() != null && !request.getDateCreated().isEmpty()) newZProj.setDateCreated(parseStringToDate(request.getDateDue()));
            newZProj.setContext(request.getContext());
            newZProj.setUserID(authToken.getUserID());
            successfulCreate = m_daoControl.createZProject(newZProj);

            result.setZPID(newZProj.getID());
            result.setZPName(newZProj.getZPName());
            result.setMessage("createZProject successful");
        }
        catch (DatabaseException e)
        {
            result.setMessage(e.getMessage());
        }
        finally
        {
            m_daoControl.closeConnection(successfulCreate);
        }
        return result;
    }

    public ReadZProjectResult readZProject(ReadZProjectRequest request)
    {
        ReadZProjectResult result = new ReadZProjectResult();
        try
        {
            m_daoControl.openConnection();

            List<ZProject> zProjects;
            if (request.getData() == null || request.getData().length == 0)
            {
                zProjects = m_daoControl.readZProjectsByUserID(request.getUserID());
                result.setData(zProjects.toArray(new ZProject[zProjects.size()]));
            }
            else
            {
                // Get desired projects
                int[] ZPIDs = request.getData();
                ZProject[] zProjectArray = new ZProject[ZPIDs.length];
                for (int i = 0; i < ZPIDs.length; i++)
                {
                    zProjectArray[i] = m_daoControl.readZProject(ZPIDs[i]);
                }
                result.setData(zProjectArray);
            }
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            // No commit because just reading. No sense in risking data corruption
            m_daoControl.closeConnection(false);
        }
        return result;
    }

    public UpdateZProjectResult updateZProject(UpdateZProjectRequest request)
    {
        UpdateZProjectResult result = new UpdateZProjectResult();
        boolean successfulUpdate = false;
        try
        {
            ZProject newZProj = new ZProject();

            m_daoControl.openConnection();

            // Check auth token and if valid, get the userID
            AuthToken authToken = m_daoControl.readAuthToken(request.getAuthToken());

            newZProj.setZPID(request.getZPID());
            newZProj.setZPName(request.getName());
            newZProj.setTimeEstimated(request.getTimeEstimated());
            newZProj.setTimeTracked(request.getTimeTracked());

            if (request.getDateDue() != null && !request.getDateDue().isEmpty()) newZProj.setDateDue(parseStringToDate(request.getDateDue()));
            if (request.getDateStart() != null && !request.getDateStart().isEmpty()) newZProj.setDateStart(parseStringToDate(request.getDateDue()));
            if (request.getDateFinished() != null && !request.getDateFinished().isEmpty()) newZProj.setDateFinished(parseStringToDate(request.getDateDue()));
            if (request.getDateCreated() != null && !request.getDateCreated().isEmpty()) newZProj.setDateCreated(parseStringToDate(request.getDateDue()));
            newZProj.setContext(request.getContext());
            newZProj.setUserID(authToken.getUserID());
            successfulUpdate = m_daoControl.updateZProject(newZProj);

            result.setZPID(newZProj.getID());
            result.setZPName(newZProj.getZPName());
            result.setMessage("updateZProject successful");
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(successfulUpdate);
        }
        return result;

    }

    public DeleteZProjectResult deleteZProject(DeleteZProjectRequest request)
    {
        DeleteZProjectResult result = new DeleteZProjectResult();
        boolean successfulDelete= false;
        try
        {
            m_daoControl.openConnection();
            // TODO validate authorization token. throw an exception of expired or invalid.
            AuthToken authToken =  m_daoControl.readAuthToken(request.getAuthToken());

            ZProject toDelete = new ZProject();
            toDelete.setZPID(request.getZPID());

            successfulDelete = m_daoControl.deleteZProject(toDelete);

            result.setSuccess(successfulDelete);
            String message = null;
            message = successfulDelete ? "Successfully deleted ZProjecte" : "Failed to delete ZProject";

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(successfulDelete);
        }
        return result;
    }

    // TODO remove method call from webAPI. Should just add authToken as a member of the request.
    public int readUserIDFromAuth(String sAuthToken)
    {
        int id = 0;
        try
        {
            m_daoControl.openConnection();
            AuthToken authToken = m_daoControl.readAuthToken(sAuthToken);
            id = authToken.getUserID();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(false);
        }
        return id;
    }

    public void setDAO(DAOControl DAO)
    {
        m_daoControl = DAO;
    }
}
