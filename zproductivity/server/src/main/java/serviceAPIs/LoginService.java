package serviceAPIs;

import comm.requests.LoginRequest;
import comm.results.LoginResult;

import dao.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import model.User;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class LoginService
{
    private DAOControl m_daoControl = new DAOControl();

    public LoginResult login(LoginRequest request)
    {
        assert request.getUserName() != null;
        assert request.getPassword() != null;
        LoginResult result = new LoginResult();

        boolean successfulLogin = false;
        try
        {
            m_daoControl.openConnection();
            if (!validUser(request.getUserName()))
            {
                result.setMessage("Invalid username");
                return result;
            }
            User usr = m_daoControl.readUser(request.getUserName());
            if (usr.getPassword().equals(request.getPassword()))
            {
                AuthToken authToken = new AuthToken(usr);
                authToken.genRandomAuthToken();
                authToken.setUserID(usr.getID());
                successfulLogin = m_daoControl.createAuthToken(authToken);
                result.setUsername(usr.getUserName());
                result.setUserID(usr.getID());
                result.setAuthToken(authToken.getAuthId());
            }
            else
            {
                result.setMessage("Invalid password");
            }
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            m_daoControl.closeConnection(successfulLogin);
        }
        return result;
    }

    private boolean validUser(String userName) {
        try
        {
            User usr = m_daoControl.readUser(userName);
            return true;
        }
        catch (DatabaseException e)
        {
            return false;
        }
    }

    public void setDAO(DAOControl DAO)
    {
        m_daoControl = DAO;
    }
}
