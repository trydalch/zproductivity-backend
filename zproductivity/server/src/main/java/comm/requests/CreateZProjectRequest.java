package comm.requests;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class CreateZProjectRequest
{
    private String name;
    private double timeEstimated;
    private double timeTracked;
    private String dateDue;
    private String dateStart;
    private String dateFinished;
    private String context;
    private String authToken;
    private String dateCreated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTimeEstimated() {
        return timeEstimated;
    }

    public void setTimeEstimated(double timeEstimated) {
        this.timeEstimated = timeEstimated;
    }

    public double getTimeTracked() {
        return timeTracked;
    }

    public void setTimeTracked(double timeTracked) {
        this.timeTracked = timeTracked;
    }

    public String getDateDue()
    {
        return dateDue;
    }

    public void setDateDue(String dateDue)
    {
        this.dateDue = dateDue;
    }

    public String getDateStart()
    {
        return dateStart;
    }

    public void setDateStart(String dateStart)
    {
        this.dateStart = dateStart;
    }

    public String getDateFinished()
    {
        return dateFinished;
    }

    public void setDateFinished(String dateFinished)
    {
        this.dateFinished = dateFinished;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getAuthToken()
    {
        return authToken;
    }

    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public String getDateCreated()
    {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated)
    {
        this.dateCreated = dateCreated;
    }
}
