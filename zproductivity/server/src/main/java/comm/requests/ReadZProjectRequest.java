package comm.requests;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class ReadZProjectRequest
{
    private int userID;
    private int[] data;

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public int[] getData()
    {
        return data;
    }

    public void setData(int[] data)
    {
        this.data = data;
    }
}
