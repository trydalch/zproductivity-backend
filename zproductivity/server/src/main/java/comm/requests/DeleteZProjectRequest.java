package comm.requests;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class DeleteZProjectRequest
{
    private int ZPID;
    private String authToken;

    public String getAuthToken()
    {
        return authToken;
    }

    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }


    public int getZPID()
    {
        return ZPID;
    }

    public void setZPID(int ZPID)
    {
        this.ZPID = ZPID;
    }
}
