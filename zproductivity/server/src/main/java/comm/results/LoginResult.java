package comm.results;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class LoginResult
{
    private String Username;
    private int UserID;
    private String authToken;
    private String message;

    public String getUsername()
    {
        return Username;
    }

    public void setUsername(String username)
    {
        Username = username;
    }

    public int getUserID()
    {
        return UserID;
    }

    public void setUserID(int userID)
    {
        UserID = userID;
    }

    public String getAuthToken()
    {
        return authToken;
    }

    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
