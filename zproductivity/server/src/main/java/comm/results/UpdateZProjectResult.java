package comm.results;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class UpdateZProjectResult
{
    private int ZPID;
    private String ZPName;
    private String message;

    public void setZPID(int ZPID)
    {
        this.ZPID = ZPID;
    }

    public void setZPName(String ZPName)
    {
        this.ZPName = ZPName;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public int getZPID()
    {
        return ZPID;
    }

    public String getZPName()
    {
        return ZPName;
    }

    public String getMessage()
    {
        return message;
    }
}
