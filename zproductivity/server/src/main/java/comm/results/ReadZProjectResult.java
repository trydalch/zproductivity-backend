package comm.results;

import model.ZProject;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class ReadZProjectResult
{
    ZProject[] data;
    String message;

    public ZProject[] getData()
    {
        return data;
    }

    public void setData(ZProject[] data)
    {
        this.data = data;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
