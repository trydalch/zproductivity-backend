package comm.results;

/**
 * Created by trevor.rydalch on 6/22/17.
 */

public class CreateZProjectResult
{
    //TODO make this a string and refactor.
    private int ZPID;
    private String ZPName;
    private String message;

    public int getZPID()
    {
        return ZPID;
    }

    public void setZPID(int ZPID)
    {
        this.ZPID = ZPID;
    }

    public String getZPName()
    {
        return ZPName;
    }

    public void setZPName(String ZPName)
    {
        this.ZPName = ZPName;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
