package WSZProductivity;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import webAPIs.WebLoginAPI;
import webAPIs.WebZActionAPI;
import webAPIs.WebZProjectAPI;
import webAPIs.DefaultHandler;
import webAPIs.DeleteZProjectHandler;
import webAPIs.ReadZProjectHandler;
import webAPIs.WebUserAPI;
import webAPIs.UpdateZProjectHandler;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class WSZproductivity
{
    private static final int MAX_WAITING_CONNECTIONS = 12;
    public static int AuthTokenTimeoutSeconds;

    private static Logger logger;

    static {
        try {
            initLog();
        }
        catch (IOException e) {
            System.out.println("Could not initialize log: " + e.getMessage());
            e.printStackTrace();
        }
    }


    private static void initLog() throws IOException {

        Level logLevel = Level.FINEST;

        logger = Logger.getLogger("ZLog");
        logger.setLevel(logLevel);
        logger.setUseParentHandlers(false);

        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(logLevel);
        consoleHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(consoleHandler);

        FileHandler fileHandler = new FileHandler("zlog.txt", false);
        fileHandler.setLevel(logLevel);
        fileHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(fileHandler);
    }


    // NON-STATIC MEMBERS
    private HttpServer server;

    private void run(String portNumber) {

        logger.info("Initializing HTTP Server");
        try {
            System.out.println("Address:" + Inet4Address.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            server = HttpServer.create(
                    new InetSocketAddress(Integer.parseInt(portNumber)),
                    MAX_WAITING_CONNECTIONS);
        }
        catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            return;
        }

        server.setExecutor(null); // use the default executor

        logger.info("Creating contexts");
        server.createContext("/", new DefaultHandler());
        server.createContext("/user", new WebUserAPI());
        server.createContext("/login", new WebLoginAPI());
        server.createContext("/ZProject", new WebZProjectAPI());
        server.createContext("/ZAction", new WebZActionAPI());
        logger.info("Starting HTTP server");
        server.start();
    }

    public static void main(String[] args) {
        String portNumber = args[0];
        new WSZproductivity().run(portNumber);
        AuthTokenTimeoutSeconds = Integer.parseInt(args[1]);


    }

}
