package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import exceptions.DatabaseException;
import io.parse.Parse.*;
import model.ZProject;

import static io.parse.Parse.parseStringToDate;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class DAOZProject
{
    private Connection m_conn;

    public Connection getConnection()
    {
        return m_conn;
    }

    public void setConnection(Connection conn)
    {
        m_conn = conn;
    }

    public boolean createZProject(ZProject zproj) throws DatabaseException
    {
        assert zproj != null;
        assert zproj.getID() != 0;
        assert m_conn != null;
        PreparedStatement stmt;
        try
        {
            assert !m_conn.isClosed();
            String sql = "INSERT INTO ZProject (ZPID, ZPName, TimeEstimated, TimeTracked, DueDate, StartDate, CreatedDate, FinishDate, Context, UserID) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, zproj.getID());
            stmt.setString(2, zproj.getZPName());
            stmt.setDouble(3, zproj.getTimeEstimated());
            stmt.setDouble(4, zproj.getTimeTracked());

            DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");

            stmt.setObject(5, (zproj.getDateDue() == null ? null : dateFormat.format(zproj.getDateDue())));
            stmt.setObject(7, (zproj.getDateStart() == null ? null : dateFormat.format(zproj.getDateStart())));
            stmt.setObject(8, (zproj.getDateCreated() == null ? null : dateFormat.format(zproj.getDateCreated())));
            stmt.setObject(9, (zproj.getDateFinished() == null ? null : dateFormat.format(zproj.getDateFinished())));
            stmt.setInt(10, zproj.getUserID());
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in createZProject", e);
        }
    }

    public ZProject readZProject(int id) throws DatabaseException
    {
        assert id > 0;
        assert m_conn != null;
        PreparedStatement stmt;
        ResultSet resultSet;
        ZProject zProject = new ZProject();
        try
        {
            assert !m_conn.isClosed();
            String sql = "SELECT * FROM ZProject WHERE ZPID =?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, id);;
            resultSet = stmt.executeQuery();

            if (!resultSet.next() || resultSet.isClosed())
            {
                return null;
            }

            DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");

            zProject.setZPID(resultSet.getInt(1));
            zProject.setZPName(resultSet.getString(2));
            zProject.setTimeEstimated(resultSet.getDouble(3));
            zProject.setTimeTracked(resultSet.getDouble(4));
            zProject.setDateDue(parseStringToDate(resultSet.getString(5)));
            zProject.setDateStart(resultSet.getDate(6));
            zProject.setDateCreated(resultSet.getDate(7));
            zProject.setDateFinished(resultSet.getDate(8));
            zProject.setContext(resultSet.getString(9));
            zProject.setUserID(resultSet.getInt(10));

            resultSet.close();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readZProject", e);
        }
        return zProject;
    }
    
    public List<ZProject> readZProjectByUserID(int userID) throws DatabaseException
    {
        assert userID != 0;
        PreparedStatement stmt;
        ResultSet resultSet;
        List<ZProject> zProjects = new ArrayList<>();
        try
        {
            String sql = "SELECT * FROM ZProject WHERE UserID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            resultSet = stmt.executeQuery();

            while (resultSet.next())
            {
                ZProject zProject = new ZProject();
                zProject.setZPID(resultSet.getInt(1));
                zProject.setZPName(resultSet.getString(2));
                zProject.setTimeEstimated(resultSet.getDouble(3));
                zProject.setTimeTracked(resultSet.getDouble(4));
                zProject.setDateDue(resultSet.getString(5) == null ? null : parseStringToDate(resultSet.getString(5)));
                zProject.setDateStart(resultSet.getDate(6));
                zProject.setDateCreated(resultSet.getDate(7));
                zProject.setDateFinished(resultSet.getDate(8));
                zProject.setContext(resultSet.getString(9));
                zProject.setUserID(resultSet.getInt(10));
                zProjects.add(zProject);

                resultSet.close();
            }
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readZProjectByUserID", e);
        }
        return zProjects;
    }

    public boolean updateZProject(ZProject zproj) throws DatabaseException
    {
        assert zproj != null;
        assert zproj.getID() != 0;
        PreparedStatement stmt;
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");

            String sql = "UPDATE ZProject SET ZPName=?, TimeEstimated=?, TimeTracked=?, DueDate=?, StartDate=?, CreatedDate=?, FinishDate=?, Context=? WHERE ZPID=? AND UserID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, zproj.getZPName());
            stmt.setDouble(2, zproj.getTimeEstimated());
            stmt.setDouble(3, zproj.getTimeTracked());
            stmt.setObject(4, (zproj.getDateDue() == null ? null : dateFormat.format(zproj.getDateDue())));
            stmt.setObject(5, (zproj.getDateStart() == null ? null : dateFormat.format(zproj.getDateStart())));
            stmt.setObject(6, (zproj.getDateCreated() == null ? null : dateFormat.format(zproj.getDateCreated())));
            stmt.setObject(7, (zproj.getDateFinished() == null ? null : dateFormat.format(zproj.getDateFinished())));
            stmt.setString(8, zproj.getContext());
            stmt.setInt(9,zproj.getID());
            stmt.setInt(10, zproj.getUserID());

            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in updateZProject", e);
        }
    }


    public boolean deleteZProject(ZProject zproj) throws DatabaseException
    {
        assert zproj != null;
        assert m_conn != null;
        PreparedStatement stmt;
        try
        {
            String sql = "DELETE FROM ZProject WHERE ZPID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, zproj.getID());
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in deleteZProject", e);
        }
    }

    public int readLastID(ZProject zProject) throws DatabaseException
    {
        int lastID = -1;
        PreparedStatement stmt;
        ResultSet resultSet;
        try
        {
            String sql = "SELECT MAX(ZPID) FROM ZProject";
            stmt = m_conn.prepareStatement(sql);
            resultSet = stmt.executeQuery();

            lastID = resultSet.getInt(1);

            resultSet.close();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readLastInt DAOZProject", e);
        }
        return lastID;
    }

}
