package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exceptions.DatabaseException;
import model.AuthToken;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class DAOAuthToken
{
    private Connection m_conn;

    public Connection getConnection()
    {
        return m_conn;
    }

    void setConnection(Connection conn)
    {
        m_conn = conn;
    }

    boolean createAuthToken(AuthToken authToken) throws DatabaseException
    {
        assert m_conn != null;
        assert authToken != null;
        assert authToken.getTimeCreated() != 0;
        assert authToken.getUserID() != 0;
        assert authToken.getAuthId() != null;

        PreparedStatement stmt;
        try
        {
            String sql = "INSERT INTO AuthToken (AuthID, UserID, TimeCreated) values (?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, authToken.getAuthId());
            stmt.setInt(2, authToken.getUserID());
            stmt.setLong(3, authToken.getTimeCreated());
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in createAuthToken", e);
        }
    }

    AuthToken readAuthToken(String sAuthID) throws DatabaseException
    {
        assert sAuthID != null;
        assert m_conn != null;
        PreparedStatement stmt;
        ResultSet resultSet;
        AuthToken authToken = new AuthToken();
        try
        {
            String sql = "SELECT * FROM AuthToken WHERE AuthID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, sAuthID);
            resultSet = stmt.executeQuery();

            authToken.setAuthId(resultSet.getString(1));
            authToken.setUserID(resultSet.getInt(2));
            authToken.setTimeCreated(resultSet.getLong(3));

            resultSet.close();
        }
        catch (SQLException e)
        {
            String errorMessage = "ERROR in readAuthTokenn";
            if (e.getMessage().equals("ResultSet closed"))
            {
                errorMessage = sAuthID == null ? "Missing Authorization Token": "Invalid Authorization Token";
            }
            throw new DatabaseException(errorMessage, e);
        }
        return authToken;
    }

    boolean deleteAuthToken(AuthToken authToken) throws DatabaseException
    {
        String id = authToken.getAuthId();
        PreparedStatement stmt = null;
        try
        {
            String sql = "DELETE FROM AuthToken WHERE AuthID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, id);
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("deleteAuthToken failed", e);
        }
    }
}
