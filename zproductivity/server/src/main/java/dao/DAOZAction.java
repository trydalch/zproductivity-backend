package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exceptions.DatabaseException;
import model.ZAction;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class DAOZAction
{
    private Connection m_conn;

    public Connection getConnection()
    {
        return m_conn;
    }

    public void setConnection(Connection conn)
    {
        m_conn = conn;
    }

    public boolean createZAction(ZAction zact) throws DatabaseException
    {
        assert zact != null;
        assert zact.getID() != 0;
        assert m_conn != null;
        PreparedStatement stmt;
        try
        {
            assert !m_conn.isClosed();
            String sql = "INSERT INTO ZAction (ZAID, ZAName, TimeEstimated, TimeTracked, DueDate, StartDate, CreatedDate, FinishDate, Context, ZParentID, UserID) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, zact.getID());
            stmt.setString(2, zact.getName());
            stmt.setDouble(3, zact.getTimeEstimated());
            stmt.setDouble(4, zact.getTimeTracked());
            stmt.setDate(5, (Date) zact.getDateDue());
            stmt.setDate(7, (Date) zact.getDateStart());
            stmt.setDate(8, (Date) zact.getDateCreated());
            stmt.setDate(9, (Date) zact.getDateFinished());
            stmt.setInt(10, zact.getParentID());
            stmt.setInt(10, zact.getUserID());
            return stmt.execute();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in createZAction", e);
        }
    }

    public ZAction readZAction(int id) throws DatabaseException
    {
        assert id > 0;
        assert m_conn != null;
        PreparedStatement stmt;
        ResultSet resultSet;
        ZAction zAction = new ZAction();
        try
        {
            assert !m_conn.isClosed();
            String sql = "SELECT * FROM ZAction WHERE ZPID =?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, id);;
            resultSet = stmt.executeQuery();

            if (!resultSet.next() || resultSet.isClosed())
            {
                return null;
            }
            zAction.setID(resultSet.getInt(1));
            zAction.setName(resultSet.getString(2));
            zAction.setTimeEstimated(resultSet.getDouble(3));
            zAction.setTimeTracked(resultSet.getDouble(4));
            zAction.setDateDue(resultSet.getDate(5));
            zAction.setDateStart(resultSet.getDate(6));
            zAction.setDateCreated(resultSet.getDate(7));
            zAction.setDateFinished(resultSet.getDate(8));
            zAction.setContext(resultSet.getString(9));
            zAction.setUserID(resultSet.getInt(10));

            resultSet.close();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readZAction", e);
        }
        return zAction;
    }

    public boolean updateZAction(ZAction zact) throws DatabaseException
    {
        assert zact != null;
        assert zact.getID() != 0;
        PreparedStatement stmt;
        try
        {
            String sql = "UPDATE ZAction SET ZAID=?, ZAName=?, TimeEstimated=?, TimeTracked=?, DueDate=?, StartDate=?, CreatedDate=?, FinishDate=?, Context=?, ZParentID, UserID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1,zact.getID());
            stmt.setString(2, zact.getName());
            stmt.setDouble(3, zact.getTimeEstimated());
            stmt.setDouble(4, zact.getTimeTracked());
            stmt.setDate(5, (Date) zact.getDateDue());
            stmt.setDate(7, (Date) zact.getDateStart());
            stmt.setDate(8, (Date) zact.getDateCreated());
            stmt.setDate(9, (Date) zact.getDateFinished());
            stmt.setInt(10, zact.getUserID());

            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in updateZAction", e);
        }
    }

    public boolean deleteZAction(ZAction zact) throws DatabaseException
    {
        assert zact != null;
        assert m_conn != null;
        PreparedStatement stmt;
        try
        {
            String sql = "DELETE FROM ZProject WHERE ZAID=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, zact.getID());
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in deleteZProject", e);
        }
    }

    public int readLastID(ZAction zAction) throws DatabaseException
    {
        int lastID = -1;
        PreparedStatement stmt;
        ResultSet resultSet;
        try
        {
            String sql = "SELECT MAX(ZAID) FROM ZAction";
            stmt = m_conn.prepareStatement(sql);
            resultSet = stmt.executeQuery();

            lastID = resultSet.getInt(1);

            resultSet.close();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readLastInt DAOZAction", e);
        }
        return lastID;
    }
}
