package dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import exceptions.DatabaseException;
import model.AuthToken;
import model.User;
import model.ZAction;
import model.ZProject;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class DAOControl
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("ZLog");
    }

    static
    {
        try
        {
            final String driver = "org.sqlite.JDBC";
            Class.forName(driver);
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private DAOUser m_daoUsr;
    private DAOAuthToken m_daoAuth;
    private DAOZProject m_daoZProject;
    private DAOZAction m_daoZAction;

    private Connection m_conn;

    private File m_DBCreateFile = new File("dbSchema.txt");
    private String m_dbName = "data" + File.separator + "db" + File.separator + "ZProductivity.sqlite";


    public String getDB()
    {
        return m_dbName;
    }

    public void setDB(String dbName)
    {
        m_dbName = dbName;
    }

    /**
     * Opens a connection to the database, then shares that connection with a
     * DAOUser object, DAOAuthToken object, DAOEvent object, and a DAOPerson object.
     *
     * @throws DatabaseException
     */
    public void openConnection() throws DatabaseException
    {
        try
        {
            final String CONNECTION_URL = "jdbc:sqlite:" + m_dbName;

            // Open a database connection
            m_conn = DriverManager.getConnection(CONNECTION_URL);

            // Start a transaction
            m_conn.setAutoCommit(false);

            m_daoUsr = new DAOUser();
            m_daoZAction = new DAOZAction();
            m_daoZProject = new DAOZProject();
            m_daoAuth = new DAOAuthToken();

            m_daoUsr.setConnection(m_conn);
            m_daoAuth.setConnection(m_conn);
            m_daoZAction.setConnection(m_conn);
            m_daoZProject.setConnection(m_conn);
            logger.info("Opened Connection to DB");
        }
        catch(SQLException e)
        {
            logger.info("Open Connection failed");
            throw new DatabaseException("openConnection failed", e);
        }
    }

    /**
     * Closes the connection opened in the openConnection method.
     *
     * Because it is shared with the DAO subclasses, they no longer have a connection to the Database.
     * @param commit
     * @throws DatabaseException
     */
    public void closeConnection(boolean commit)
    {
        try
        {
            if (commit)
            {
                m_conn.commit();
            }
            else
            {
                m_conn.rollback();
            }

            m_conn.close();
            logger.info("Closed Connection to DB");
            m_conn = null;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            logger.info("Close Connection failed");
        }
    }

    public int readLastID(Object object) throws DatabaseException
    {
        if (object instanceof User)
        {
            return m_daoUsr.readLastID((User) object);
        }
        else if (object instanceof ZProject)
        {
            return m_daoZProject.readLastID((ZProject) object);
        }
        else if (object instanceof ZAction)
        {
            return m_daoZAction.readLastID((ZAction) object);
        }
        else
        {
            return -1;
        }
    }

    public boolean createUser(User usr) throws DatabaseException
    {
        return m_daoUsr.createUser(usr);
    }

    public User readUser(String usrName) throws DatabaseException
    {
        return m_daoUsr.readUser(usrName);
    }

    public boolean updateUser(User usr) throws DatabaseException
    {
        return m_daoUsr.updateUser(usr);
    }

    public boolean deleteUser(User usr) throws DatabaseException
    {
        return m_daoUsr.deleteUser(usr);
    }

    public boolean createAuthToken(AuthToken authToken) throws DatabaseException
    {
        return  m_daoAuth.createAuthToken(authToken);
    }

    public AuthToken readAuthToken(String sAuthToken) throws DatabaseException
    {
        return m_daoAuth.readAuthToken(sAuthToken);
    }

    public boolean deleteAuthToken(AuthToken authToken) throws DatabaseException
    {
        return m_daoAuth.deleteAuthToken(authToken);
    }

    public boolean createZProject(ZProject zproj) throws DatabaseException
    {
        return m_daoZProject.createZProject(zproj);
    }

    public ZProject readZProject(int id) throws DatabaseException
    {
        return m_daoZProject.readZProject(id);
    }

    public List<ZProject> readZProjectsByUserID(int userID) throws DatabaseException
    {
        return m_daoZProject.readZProjectByUserID(userID);
    }

    public boolean updateZProject(ZProject zproj) throws DatabaseException
    {
        return m_daoZProject.updateZProject(zproj);
    }

    public boolean deleteZProject(ZProject zproj) throws DatabaseException
    {
        return m_daoZProject.deleteZProject(zproj);
    }

    public boolean createZAction(ZAction zact) throws DatabaseException
    {
        return m_daoZAction.createZAction(zact);
    }

    public ZAction readZAction(int id) throws DatabaseException
    {
        return  m_daoZAction.readZAction(id);
    }

    public boolean updateZAction(ZAction zact) throws DatabaseException
    {
        return m_daoZAction.updateZAction(zact);
    }

    public boolean deleteZAction(ZAction zact) throws DatabaseException
    {
        return m_daoZAction.deleteZAction(zact);
    }

}
