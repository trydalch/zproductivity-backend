package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exceptions.DatabaseException;
import model.User;

/**
 * Created by Trevor.Rydalch on 6/22/2017.
 */

public class DAOUser
{
    private Connection m_conn;

    public Connection getConnection()
    {
        return m_conn;
    }

    void setConnection(Connection conn)
    {
        m_conn = conn;
    }

    boolean createUser(User usr) throws DatabaseException
    {
        assert m_conn != null;
        assert usr != null;
        assert usr.getID() != 0;

        PreparedStatement stmt;
        try
        {
            String sql = "INSERT INTO User (UserID, Username, Password, Email, FirstName, LastName, Gender, Age)" +
                        "VALUES (?,?,?,?,?,?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setInt(1, usr.getID());
            stmt.setString(2, usr.getUserName());
            stmt.setString(3, usr.getPassword());
            stmt.setString(4, usr.getEmail());
            stmt.setString(5, usr.getFirstName());
            stmt.setString(6, usr.getLastName());
            stmt.setString(7, usr.getGender());
            stmt.setInt(8, usr.getAge());
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            String errorMessage;
            if (e.getMessage().contains("Username is not unique"))
            {
                errorMessage = "Username is taken";
            }
            else
            {
                errorMessage = "Error in DAOUser";
            }
            throw new DatabaseException(errorMessage, e);
        }
    }

    User readUser(String usrName) throws DatabaseException
    {
        assert m_conn != null;
        assert usrName != null;
        assert !usrName.isEmpty();
        PreparedStatement stmt;
        ResultSet resultSet;
        User usr = new User();
        try
        {
            String sql = "SELECT UserID, Username, Password, Email, FirstName, LastName, Gender, Age FROM User WHERE Username =?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, usrName);
            resultSet = stmt.executeQuery();

            usr.setID(resultSet.getInt(1));
            usr.setUserName(resultSet.getString(2));
            usr.setPassword(resultSet.getString(3));
            usr.setEmail(resultSet.getString(4));
            usr.setFirstName(resultSet.getString(5));
            usr.setLastName(resultSet.getString(6));
            usr.setGender(resultSet.getString(7));
            usr.setAge(resultSet.getInt(8));

            resultSet.close();
            stmt.close();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readUser", e);
        }
        return usr;
    }

    boolean updateUser(User usr) throws DatabaseException
    {
        assert usr != null;
        assert usr.getID() != 0;
        assert m_conn != null;
        PreparedStatement updateStatment;
        try
        {
            String sqlUpdate = "UPDATE User SET Username=?, Password=?, Email=?, FirstName=?, LastName=?, Gender=?, Age=?" +
                                "WHERE userID=?";
            updateStatment = m_conn.prepareStatement(sqlUpdate);
            updateStatment.setString(1, usr.getUserName());
            updateStatment.setString(2, usr.getPassword());
            updateStatment.setString(3, usr.getEmail());
            updateStatment.setString(4, usr.getFirstName());
            updateStatment.setString(5, usr.getLastName());
            updateStatment.setString(6, usr.getGender());
            updateStatment.setInt(7, usr.getAge());

            return updateStatment.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in updateUser", e);
        }
    }


    boolean deleteUser(User usr)
    {
        assert usr != null;
        assert usr.getID() != 0;
        assert m_conn != null;
        PreparedStatement stmt;
        try
        {
            String sql = "DELETE FROM User WHERE Username=?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, usr.getUserName());
            return stmt.executeUpdate() == 1;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public int readLastID(User usr) throws DatabaseException
    {
        int lastID = -1;
        PreparedStatement stmt;
        ResultSet resultSet;
        try
        {
            String sql = "SELECT MAX(UserID) FROM User";
            stmt = m_conn.prepareStatement(sql);
            resultSet = stmt.executeQuery();

            lastID = resultSet.getInt(1);

            resultSet.close();
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Error in readLastInt DAOUser", e);
        }
        return lastID;
    }
}
