package exceptions;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class DatabaseException extends Exception
{
    public DatabaseException(String s)
    {
        super(s);
    }

    public DatabaseException(String s, Throwable throwable)
    {
        super(s, throwable);
    }
}
