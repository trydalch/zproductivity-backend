package exceptions;

/**
 * Created by Trevor.Rydalch on 6/23/2017.
 */

public class ServiceException extends Exception
{
    public ServiceException(String s) { super(s);}

    public ServiceException(String s, Throwable throwable) { super(s, throwable); }
}
